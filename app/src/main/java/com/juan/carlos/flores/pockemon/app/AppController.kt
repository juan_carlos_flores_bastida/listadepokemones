package com.juan.carlos.flores.pockemon.app

import android.app.Application
import android.content.Context
import com.juan.carlos.flores.pockemon.data.api.PokemonClient.Companion.createClient
import com.juan.carlos.flores.pockemon.data.api.PokemonClientI
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class AppController: Application() {

    private var pokemonService: PokemonClientI? = null
    private var scheduler: Scheduler? = null

    val _pokemonService: PokemonClientI
    get() {
        if (pokemonService == null) {
            pokemonService = createClient()
        }
        return pokemonService!!
    }

    fun subscribeScheduler(): Scheduler {
        if (scheduler == null) {
            scheduler = Schedulers.io()
        }
        return scheduler!!
    }

    companion object {
        private operator fun get(context: Context): AppController {
            return context.applicationContext as AppController
        }

        fun create(context: Context): AppController {
            return get(context)
        }
    }
}
package com.juan.carlos.flores.pockemon.data.api

import com.juan.carlos.flores.pockemon.data.vo.PokemonResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonClientI {

    @GET("type/{type}")
    fun getPokemonType(@Path("type") type: Int): Observable<PokemonResponse>
}
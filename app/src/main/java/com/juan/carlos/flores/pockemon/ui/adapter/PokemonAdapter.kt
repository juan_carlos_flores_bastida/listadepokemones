package com.juan.carlos.flores.pockemon.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.juan.carlos.flores.pockemon.R
import com.juan.carlos.flores.pockemon.data.vo.Pokemon
import com.juan.carlos.flores.pockemon.databinding.PokemonItemViewBinding
import com.juan.carlos.flores.pockemon.viewmodel.ItemPokemonViewModel
import com.juan.carlos.flores.pockemon.viewmodel.PokemonViewModel

class PokemonAdapter: RecyclerView.Adapter<PokemonAdapter.Companion.PokemonAdapterViewHolder> {

    private var pokemonList: ArrayList<Pokemon>? = null

    constructor() {
        this.pokemonList = arrayListOf()
    }

    fun setPokemonList(pokemonList: ArrayList<Pokemon>) {
        this.pokemonList = pokemonList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonAdapterViewHolder {
        var pokemonItemViewBinding = DataBindingUtil.inflate<PokemonItemViewBinding>(
            LayoutInflater.from(
                parent.context
            ), R.layout.pokemon_item_view, parent, false
        )

        return PokemonAdapterViewHolder(pokemonItemViewBinding)
    }

    override fun getItemCount(): Int {
        return pokemonList!!.size
    }

    override fun onBindViewHolder(holder: PokemonAdapterViewHolder, position: Int) {
        holder.bindPokemon(pokemonList!![position])
    }

    companion object {
        class PokemonAdapterViewHolder : RecyclerView.ViewHolder {
            var mItem: PokemonItemViewBinding? = null

            constructor(item: PokemonItemViewBinding) : super(item.pokemonItem) {
                this.mItem = item
            }

            fun bindPokemon(pokemon: Pokemon) {
                if (mItem?.pokemonViewModel == null) {
                    mItem?.pokemonViewModel = ItemPokemonViewModel(pokemon)
                } else {
                    mItem?.pokemonViewModel?.pokemon = pokemon
                }
            }
        }
    }
}
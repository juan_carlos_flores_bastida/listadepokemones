package com.juan.carlos.flores.pockemon.viewmodel

import android.view.View
import androidx.databinding.BaseObservable
import com.juan.carlos.flores.pockemon.data.vo.Pokemon

class ItemPokemonViewModel(var pokemon: Pokemon): BaseObservable() {

    fun getName(): String {
        return "Nombre: ${pokemon.pokemon.name}"
    }

    fun getUrl(): String {
        return "Url: ${pokemon.pokemon.url}"
    }

    fun onItemClick(v: View) {
        println("Click en item pokemon: ${pokemon.pokemon.name}")

    }
}
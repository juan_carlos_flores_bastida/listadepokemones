package com.juan.carlos.flores.pockemon.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juan.carlos.flores.pockemon.R
import com.juan.carlos.flores.pockemon.databinding.FragmentPokemonListBinding
import com.juan.carlos.flores.pockemon.ui.adapter.PokemonAdapter
import com.juan.carlos.flores.pockemon.viewmodel.PokemonViewModel
import java.util.*

class PokemonListFragment : Fragment(), Observer {

    var pokemonListBinding: FragmentPokemonListBinding? = null
    var pokemonViewModel: PokemonViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        setUoListOfPokemonListView(pokemonListBinding?.recyclerviewPokemonList)
        setUpObserver(pokemonViewModel)
    }

    private fun setUpObserver(observable: Observable?) {
        observable?.addObserver(this)
    }

    private fun setUoListOfPokemonListView(recyclerviewPokemonList: RecyclerView?) {
        var adapter = PokemonAdapter()
        recyclerviewPokemonList?.adapter = adapter
        recyclerviewPokemonList?.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun initBinding() {
        pokemonListBinding =
            DataBindingUtil.setContentView(requireActivity(), R.layout.fragment_pokemon_list)
        pokemonViewModel = PokemonViewModel(requireContext())
        pokemonListBinding!!.pokemonViewModel = pokemonViewModel
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is PokemonViewModel) {
            val adapter: PokemonAdapter = pokemonListBinding?.recyclerviewPokemonList?.adapter as PokemonAdapter
            val pokemonViewModel: PokemonViewModel = o
            adapter.setPokemonList(pokemonViewModel.getPokemonList()!!)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        pokemonViewModel?.reset()
    }
}
package com.juan.carlos.flores.pockemon.viewmodel

import android.content.Context
import android.view.View
import androidx.annotation.NonNull
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.juan.carlos.flores.pockemon.R
import com.juan.carlos.flores.pockemon.app.AppController
import com.juan.carlos.flores.pockemon.data.api.POKEMON_TYPE
import com.juan.carlos.flores.pockemon.data.vo.Pokemon
import com.juan.carlos.flores.pockemon.data.vo.PokemonResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.*
import kotlin.collections.ArrayList

class PokemonViewModel(@NonNull val context: Context): Observable() {

    var progressBar: ObservableInt? = null
    var recyclerView: ObservableInt? = null
    var messageLabel: ObservableInt? = null
    var errorMessage: ObservableField<String>? = null

    var _context: Context? = null

    private var pokemonList: ArrayList<Pokemon>? = null
    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()

    init {
        this._context = context
        this.pokemonList = arrayListOf()
        progressBar= ObservableInt(View.VISIBLE)
        recyclerView = ObservableInt(View.GONE)
        messageLabel = ObservableInt(View.GONE)
        errorMessage = ObservableField(_context!!.getString(R.string.charge_information))

        initializeViews()
        fetchPokemonList()
    }

    private fun fetchPokemonList() {
        var appController = AppController.create(_context!!)
        var pokemonService = appController._pokemonService

        var disposable: Disposable = pokemonService?.getPokemonType(POKEMON_TYPE)!!
            .subscribeOn(appController?.subscribeScheduler())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(Consumer<PokemonResponse> { getPokemon->
                updatePokemonList(getPokemon.pokemonList)
                progressBar?.set(View.GONE)
                messageLabel?.set(View.GONE)
                recyclerView?.set(View.VISIBLE)
            }, Consumer<Throwable> {
                errorMessage?.set(_context?.getString(R.string.error_message_loading_users))
                progressBar?.set(View.GONE)
                messageLabel?.set(View.VISIBLE)
                recyclerView?.set(View.GONE)
            })
        compositeDisposable?.add(disposable)
    }

    private fun updatePokemonList(list: List<Pokemon>) {
        pokemonList?.addAll(list)
        setChanged()
        notifyObservers()
    }

    fun getPokemonList(): ArrayList<Pokemon>? {
        return pokemonList
    }


    private fun initializeViews() {
        messageLabel?.set(View.VISIBLE)
        recyclerView?.set(View.GONE)
        progressBar?.set(View.VISIBLE)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null) {
            compositeDisposable?.dispose()
        }
    }
    
    fun reset() {
        unSubscribeFromObservable()
        compositeDisposable = null
        _context = null
    }
}
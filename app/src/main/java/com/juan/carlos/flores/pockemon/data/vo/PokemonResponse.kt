package com.juan.carlos.flores.pockemon.data.vo

import com.google.gson.annotations.SerializedName

data class PokemonResponse(
    @SerializedName("pokemon")
    val pokemonList: List<Pokemon>
)